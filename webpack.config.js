const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const build = path.resolve(__dirname, './public')
const js = path.resolve(__dirname, './src')
const sass = path.resolve(__dirname, './sass')

const configJS = {
  entry: {
    client: js + '/client',
    plugins: js + '/plugins'
  },
  output: {
    path: build + '/js',
    publicPath: build,
    filename: '[name].js',
    chunkFilename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        loader: 'babel-loader',
        include: js,
        exclude: /node_modules/
      }
    ]
  },
  optimization: {
    runtimeChunk: {
      name: 'manifest'
    },
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          priority: -20,
          chunks: 'all'
        }
      }
    }
  },
  target: 'web',
  cache: true
}

const configCSS = {
  entry: sass + '/main.scss',
  output: {
    path: build + '/css',
    publicPath: build
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        loader: 'babel-loader',
        include: js,
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader']
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'main.css'
    })
  ]
}

module.exports = [configJS, configCSS]
