import NotFound from './components/404'
import Catalog from './containers/catalog'

const Routes = [
  { path: '/catalog', exact: true, component: Catalog },
  { path: '*', component: NotFound }
]

export default Routes
