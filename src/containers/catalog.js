import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Draggable, Droppable } from 'react-drag-and-drop'
import Model from '../components/model'
import Part from '../components/part'
import Carousel from '../components/carousel'
import {
  getCatalogList,
  dragCatalogItem
} from '../redux/actions'

@connect(state=>(state))
class Catalog extends PureComponent {
  constructor() {
    super()
  }

  componentDidMount() {
    this.props.dispatch(getCatalogList())
  }

  handleDropItems(item, target) {
    this.props.dispatch(dragCatalogItem(item, target))
  }

  handleDropWrapper(item) {
    const key = Object.keys(item).find(key => item[key])
    const list = JSON.parse(item[key])
    this.handleDropItems(list, list.type)
  }

  render() {
    const { history, catalog: { list } } = this.props
    const body = list.find(list => list.tag === 'body')
    const full = list.find(list => list.tag === 'full')
    const footer = list.find(list => list.tag === 'footer')

    const Parts = [body, full, footer].map((part, index) => {
      if (!part)
        return <div key={`div-part-${index}`}/>

      return (
        <Part
          key={`div-part-${index}`}
          name={part.type}
          className={part.type}
          data = {part}
          onDrop={ this.handleDropItems.bind(this) }
        />
      )
    })

    return (
      <div className="catalog">
        <div className='model'>
          <Model />
          { Parts }
          <Droppable
            className='main-drop'
            types={ ['footer', 'body', 'full'] }
            onDrop={ this.handleDropWrapper.bind(this) }
          />
        </div>
        <div className='closet'>
          <div className='carousel'>
            <Carousel
              className = ''
              style = {{}}
              data={ list }
              types={['footer', 'body', 'full']}
              onDrop={ this.handleDropItems.bind(this) }
            />
          </div>
        </div>
      </div>
    )
  }
}

export default Catalog
