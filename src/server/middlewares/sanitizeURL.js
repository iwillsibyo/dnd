function sanitizeURL(req, res, next) {
  if (req.url === '/') {
    res.redirect(308, '/catalog')
  } else if (req.url.substr(req.url.length - 1) === '/') {
    res.redirect(308, req.url.replace(/\/{1,}$/, ''))
  } else {
    next()
  }
}

export default sanitizeURL
