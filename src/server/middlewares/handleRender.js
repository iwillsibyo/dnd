import React from 'react'
import { createStore } from 'redux'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router'
import { renderRoutes } from 'react-router-config'

import Routes from '../../routes'
import reducers from '../../redux/reducers'

function renderFullPage(html, preloadedState) {
  return `
    <!doctype html>
    <html class="no-js" lang="">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <title>Test</title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <link rel="shortcut icon" type="image/ico" href="/img/favicon.png"/>

            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
            <link rel="stylesheet" href="/css/main.css">
            <link rel="stylesheet" href="/css/fontawesome-all.css">
        </head>
        <body>
            <div id="root">${html}</div>
            <script type="text/javascript">
              window.__PRELOADED_STATE__ =
              ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
            </script>
            <script type="text/javascript" src="/js/manifest.js"></script>
            <script type="text/javascript" src="/js/client.js"></script>
            <script type="text/javascript" src="/js/plugins.js"></script>
            <script type="text/javascript" src="/js/vendors.js"></script>
        </body>
    </html>
  `
}

function handleRender(req, res) {
  const context = {}
  // Create a new Redux store instance
  const store = createStore(reducers)

  // Render the component to a string
  const html = renderToString(
    <Provider store={store}>
      <StaticRouter
        location={req.url}
        context={context}
      >
        {renderRoutes(Routes)}
      </StaticRouter>
    </Provider>
  )

  // Grab the initial state from our Redux store
  const preloadedState = store.getState()

  if (context.url) {
    console.log('@context', context.url)
    res.writeHead(301, {
      Location: context.url
    })
    res.end()
  } else {
    res.send(renderFullPage(html, preloadedState))
  }
}

export default handleRender
