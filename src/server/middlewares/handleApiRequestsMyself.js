
function handleApiRequestsMyself(req, res, next) {
  if (req.get('target') === 'selfAPI') {
  	return res.send('caught ya')
  }
  return next()
}

export default handleApiRequestsMyself
