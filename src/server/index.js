import express from 'express'
import http from 'http'
import socket_io from 'socket.io'

import { redirectUnauthorizedRequest
  , handleApiRequestsMyself
  , handleRender
  , sanitizeURL } from './middlewares'
import socket from './socket'

const app = express()
const server = http.createServer(app)
const io = socket_io(server)

socket(io)

app.use(express.static('public'))

app.use(handleApiRequestsMyself)
app.use(redirectUnauthorizedRequest)
app.get('*', [sanitizeURL, handleRender])

const PORT = process.env.PORT || 3000
server.listen(PORT, function () {
  console.log('app running @' + process.env.NODE_ENV + ' http://localhost:' + PORT)
})
