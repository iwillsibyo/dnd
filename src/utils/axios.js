import axios from 'axios'
import {
  ERROR,
  ERROR_401
} from '../redux/types'

export default (config, type) => {
  let headers = config.headers || {}

  headers = { ...headers, target: 'API' }

  Object.assign(config, {
    headers,
    withCredentials: true,
    validateStatus(status) {
      return status >= 200 && status < 500
    }
  })

  if (type) {
    return axios(config)
      .then(result => {
        if (result.status >= 400) {
          result.data.status = result.status
          return {
            type: result.status === 401 ? ERROR_401 : ERROR,
            payload: result.data
          }
        }

        return { type, payload: result.data }
      })
  }

  return axios(config)
}
