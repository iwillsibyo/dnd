const datePickerFormat = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric'
}


module.exports = {
  datePickerFormat
}
