import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { FontIcon, ListItem, List, Tooltipped } from 'react-md'

function recurse(navItems, match) {
  return navItems.map(({ label: primaryText, to, icon, routes }, index) => {
    const leftIcon = <FontIcon>{icon}</FontIcon>
    const props = {
      to,
      leftIcon,
      primaryText,
      component: RouterLink,
      active: match === to
    }

    if (Array.isArray(routes)) {
      return recurse(routes, match)
    }

    return (<Tooltipped label={primaryText} position='right' key={index}>
      <ListItem { ...props } />
    </Tooltipped>)
  })
}

function constructMiniMenu(navItems, match) {
  return (<List style={{ marginTop: 64 }}>
    {recurse(navItems, match)}
  </List>)
}

function constructMenu(navItems, match) {
  return navItems.map(({ label: primaryText, to, icon, routes }, index) => {
    const leftIcon = <FontIcon>{icon}</FontIcon>
    const props = {
      to,
      leftIcon,
      primaryText,
      component: RouterLink,
      active: match === to
    }

    if (Array.isArray(routes)) {
      delete props.to
      delete props.component
      props.nestedItems = constructMenu(routes, match)
    }

    return <ListItem { ...props } key={index} />
  })
}

module.exports = { constructMenu, constructMiniMenu }
