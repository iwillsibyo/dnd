'use strict'

module.exports = {
  sort(arr, key, ascending) {
    const multiplier = ascending ? 1 : -1
    return [...arr].sort((prev, curr) => {
      const a = String(prev[key])
      const b = String(curr[key])
      return a.localeCompare(b) * multiplier
    })
  },
  handleState(state, { type, payload }, handlers) {
    if (!handlers[type]) {
      return state
    }
    return handlers[type](state, payload)
  },
  createTypes(arr) {
    return arr.reduce((obj, key)=>({ ...obj, [key]: key.toUpperCase() }), {})
  }
}
