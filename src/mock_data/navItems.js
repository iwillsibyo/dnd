const navItems = [{
  exact: true,
  label: 'Dashboard',
  to: '/portal',
  icon: 'dashboard'
}, {
  label: 'Company',
  icon: 'domain',
  to: '/portal/company'
}, {
  label: 'User',
  to: '/portal/user',
  icon: 'account_box'
}, {
  label: 'Messages',
  to: '/portal/message',
  icon: 'chat'
}, {
  label: 'Settings',
  icon: 'settings',
  routes: [{
    label: 'Role',
    to: '/portal/settings/role',
    icon: 'streetview'
  }]
}]

export default navItems
