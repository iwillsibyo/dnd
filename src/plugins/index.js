/*
	In case we have a dependency that has no CommonJS or ES6 Module counterpart,
	just import the script here and it will be available globally. Still, this
	is highly discouraged as this is anti pattern.
*/

import './sample'
