import {
  REQUESTED,
  GET_CATALOG_LIST,
  DRAG_CATALOG_ITEM
} from '../types'

module.exports = {
  getCatalogList() {
    return {
      type: GET_CATALOG_LIST + REQUESTED,
    }
  },

  dragCatalogItem(item, target) {
    return {
      type: DRAG_CATALOG_ITEM,
      payload: { item, target }
    }
  }
}
