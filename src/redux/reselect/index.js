import { createSelector } from 'reselect'

const errorReducer = (state) => state.visibilityFilter

export const baseSelector = createSelector(
  [ errorReducer ],
  (error) => {
  	return error
  }
)
