import { handleState } from '../../utils/baseTools'

const INITIAL_STATE = {
  list: [],
}

const handlers = {
  GET_CATALOG_LIST: (state, payload) => {
    return { ...state, list: payload }
  },

  DRAG_CATALOG_ITEM: (state, { item, target }) => {
    let modify = state.list.filter(list => list.id !== item.id)

    modify = modify.map(list => {
      let tag = 'carousel'

      const find = modify.find(m => m.tag === list.type)
      const full = modify.find(m => m.tag === 'full')

      if (item.type !== 'full' && (find && find.id === list.id) && list.type !== item.type && !full)
        tag = list.type

      return { ...list, tag }
    })

    item.tag = item.type
    if (target === 'carousel')
      item.tag = 'carousel'
    
    return { ...state, list: [ ...modify, item ].sort(({ id: a }, { id: b }) => a - b) }
  }
}

module.exports = (state = INITIAL_STATE, action) => {
  return handleState(state, action, handlers)
}
