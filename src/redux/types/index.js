import base from './base'
import catalog from './catalog'

import { createTypes } from '../../utils/baseTools'

module.exports = createTypes([
  ...base,
  ...catalog
])
