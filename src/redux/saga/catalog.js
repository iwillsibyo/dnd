import { call, put, takeLatest } from 'redux-saga/effects'
import axios from '../../utils/axios'
import {
  REQUESTED,
  GET_CATALOG_LIST
} from '../types'

const entity = 'note'

function *getCatalogList() {
  const config = {
    method: 'get',
    url: `http://localhost:3006/catalog`
  }

  const result = yield call(axios, config, GET_CATALOG_LIST)
  yield put(result)
}

module.exports = function *() {
  // GET
  yield takeLatest(GET_CATALOG_LIST + REQUESTED, getCatalogList)
}
