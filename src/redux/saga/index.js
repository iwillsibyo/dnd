import { fork, all } from 'redux-saga/effects'
import catalog from './catalog'

export default function *root() {
  yield all([
    fork(catalog)
  ])
}
