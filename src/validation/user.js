import Validator from 'validator'
import _ from 'lodash'


const requiredUserItems = ['first_name', 'last_name', 'email']

function checkErrors(key, value, errors, requiredItems) {
  let val = value
  if (typeof value === 'string') {
    val = val.trim()
  }

  if (requiredItems.includes(key) && Validator.isEmpty(val)) {
    errors[key] = 'This field is required'
  } else if (key === 'email' && !Validator.isEmail(val)) {
    errors[key] = 'Email is invalid'
  } else {
    delete errors[key]
  }
}

function validateForm(data) {
  let errors = {}
  for (const key in data) {
    checkErrors(key, data[key], errors, requiredUserItems)
  }
  return {
    errors,
    isValid: _.isEmpty(errors)
  }
}

function validateInput(data, errors, currentKey) {
  for (const key in data) {
    if (key === currentKey) {
      checkErrors(key, data[key], errors, requiredUserItems)
    }
  }
  return errors
}

module.exports = {
  validateForm,
  validateInput
}
