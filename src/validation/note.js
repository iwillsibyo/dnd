import Validator from 'validator'
import _ from 'lodash'


const requiredUserItems = ['note_message']
const stringValues = ['note_message']

function checkErrors(key, value, errors, requiredItems) {
  let val = value
  if (stringValues.includes(key)) {
    val = val.trim()
  }

  if (requiredItems.includes(key) && Validator.isEmpty(val)) {
    errors[key] = 'This field is required'
  } else if (key === 'email' && !Validator.isEmail(val)) {
    errors[key] = 'Email is invalid'
  } else {
    delete errors[key]
  }
}

function validateForm(data) {
  let errors = {}
  for (const key in data) {
    checkErrors(key, data[key].trim(), errors, requiredUserItems)
  }
  return {
    errors,
    isValid: _.isEmpty(errors)
  }
}

function validateInput(data, errors, currentKey) {
  for (const key in data) {
    if (key === currentKey) {
      checkErrors(key, data[key].trim(), errors, requiredUserItems)
    }
  }
  return errors
}

module.exports = {
  validateForm,
  validateInput
}
