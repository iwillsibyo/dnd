import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Draggable, Droppable } from 'react-drag-and-drop'
import AliceCarousel from 'react-alice-carousel';
import gallary from './gallary'


const entity = 'Draft'

class Carousel extends PureComponent {

  constructor(props) {
    super(props)
  }

  handleDropItems(item) {
    const key = Object.keys(item).find(key => item[key])
    this.props.onDrop(JSON.parse(item[key]), 'carousel')
  }

  handleDoubleClick(item) {
    this.props.onDrop(item, item.type)
  }

  render() {
    const {
      className = '',
      style = {},
      data,
      types
    } = this.props

    const responsive = {
      0: { items: 1 },
      600: { items: 2  },
      1024: { items: 3 }
    };

    return (
      <div className={className} style={style}>
        <Droppable
          types={ types }
          onDrop={ this.handleDropItems.bind(this) }
        >
          <AliceCarousel
            duration={400}
            autoPlay={true}
            startIndex = {1}
            fadeOutAnimation={true}
            mouseDragEnabled={false}
            playButtonEnabled={true}
            stopAutoPlayOnHover={true}
            responsive={responsive}
            autoPlayInterval={2000}
            autoPlayDirection="rtl"
            autoPlayActionDisabled={true}
            items={gallary({
              data: data,
              onDoubleClick: this.handleDoubleClick.bind(this)
            })}
          />
        </Droppable>
      </div>
    );
  }
}

export default Carousel
