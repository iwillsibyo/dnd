import React from 'react'

import { Dialog, Button, Toolbar, CardText } from 'react-md'

const entity = '404'

function handleNavigate(history) {
  history.push('/catalog')
}

const NotFound = ({ history })=>(<div>
  <div className='container-404'>
    <img className='img-404' src='/img/404-error.svg' />
  </div>
  <Dialog id='static-dialog' containFocus={false} aria-labelledby='Page Not Found' className='md-background--card dialog-404'>
    <Toolbar title={'Uh Oh!'} className='md-divider-border md-divider-border--bottom'
      actions={<Button icon onClick={handleNavigate.bind(this, history)}>close</Button>}
    />
    <CardText>
      <p>The Page you were looking for doesn't exist.
      You might have typed in the wrong address or the page has moved.</p>
    </CardText>
    <Toolbar actions={<Button flat
      swapTheming
      key={`confirm-${entity}`}
      className='btn-right-margin'
      onClick={handleNavigate.bind(this, history)}
      primary>
        go back
    </Button>} />
  </Dialog>
</div>)

export default NotFound
