import React, { PureComponent } from 'react'
import { Draggable, Droppable } from 'react-drag-and-drop'


const gallary = ({ data, onDoubleClick }) => {
  return data.map(each => (
    <Draggable type={each.type} data={JSON.stringify(each)}>
      <img
     	className='image-handler'
        src={each.image}
        onDoubleClick={() => onDoubleClick(each)}
      />
    </Draggable>
  ))
}

export default gallary
