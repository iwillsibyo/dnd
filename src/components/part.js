import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Draggable, Droppable } from 'react-drag-and-drop'
const entity = 'Draft'

class Part extends PureComponent {
  constructor(props) {
    super(props)
  }

  handleDropItems(item) {
    const { onDrop, name } = this.props
    const key = Object.keys(item).find(key => item[key])
    onDrop(JSON.parse(item[key]), name)
  }

  handleDoubleClick(item) {
    this.props.onDrop(item, 'carousel')
  }

  render() {

    const {
      name = '',
      data = {},
      className = ''
    } = this.props

    return (
      <div className={className}>
        <Droppable
          types={ ['footer', 'body', 'full'] }
          onDrop={ this.handleDropItems.bind(this) }
        >
          <Draggable type={ data.type } data={ JSON.stringify(data) }>
            <div
              style={ data.style }
              onDoubleClick={ this.handleDoubleClick.bind(this, data) }
            >
              { data.image && <img className='image-handler' src={ data.image } />  }
            </div>
          </Draggable>
        </Droppable>
      </div>
    )
  }
}

export default Part
