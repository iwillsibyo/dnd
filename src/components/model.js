import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Draggable, Droppable } from 'react-drag-and-drop'
const entity = 'Draft'

class Model extends PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="model-container">
        <div className="">
          <img className='image-handler' src="img/catalog/model.png" /> 
        </div>
      </div>
    )
  }
}

export default Model
