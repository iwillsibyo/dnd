import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import Routes from './routes'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducers from './redux/reducers'
import sagas from './redux/saga'

import createSocketIoMiddleware from 'redux-socket.io'
import io from 'socket.io-client'
const socket = io('http://localhost:3003')

const socketIoMiddleware = createSocketIoMiddleware(socket, 'server/')
const sagaMiddleware = createSagaMiddleware()

const preloadedState = window.STATE_FROM_SERVER
delete window.STATE_FROM_SERVER

let store = createStore(
  reducers,
  preloadedState,
  applyMiddleware(sagaMiddleware, socketIoMiddleware)
)

sagaMiddleware.run(sagas)

hydrate(
  <Provider store={store}>
    <BrowserRouter>
      {renderRoutes(Routes)}
    </BrowserRouter>
  </Provider>, document.getElementById('root'))
