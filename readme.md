# React Template

An on going attempt at creating simple template for react. Aimed to become fully functional with the right tools but di pa kaayo ko kamao so study2 sah nya tapal tapal.

## Technology Stack

* Node.js
* Express.js
* React
* React-md
* Redux
* Redux Saga
* Socket.io

### Installing

Just install dependencies

```
npm i
```
## Running on Development Mode

```
npm start
```

## Deployment

Wala pa oi

## SASS

sass files are automatically compiled to css, minified, and prefixed

## Contributing

himo lang pull request


## Author

* **Jairus Ralf D. Daclan** - *Initial work* - [gitlab](https://gitlab.com/jairus.jsx)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* imo mama
