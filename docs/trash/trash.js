let newObject = Object.keys(myObject).reduce(function (previous, current) {
  previous[current] = myObject[current] * myObject[current]
  return previous
}, {})

console.log(newObject)
// => { 'a': 1, 'b': 4, 'c': 9 }

console.log(myObject)
// => { 'a': 1, 'b': 2, 'c': 3 }
