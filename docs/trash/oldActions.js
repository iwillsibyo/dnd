import {
  CLEAR_ERROR,
  REQUESTED
} from '../types'

module.exports = {
  ClearError() {
    return {
      type: CLEAR_ERROR,
      payload: null
    }
  },

  AddText() {
    return {
      type: 'ADD_TEXT'
    }
  },

  UserList() {
    return {
      type: 'GET_USER_LIST' + REQUESTED
    }
  },

  TestSocket() {
    console.log('action: socket event')
    return {
      type: 'server/fuck',
      data: 'haha'
    }
  },

  ClearMessage() {
    return {
      type: 'CLEAR_MESSAGE'
    }
  },

  HelloAPI() {
    return {
      type: 'HELLO_API' + REQUESTED
    }
  }
}
