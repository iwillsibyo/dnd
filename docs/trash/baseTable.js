import React, { PureComponent } from 'react'
import $ from 'jquery'

import {
  DataTable,
  TableHeader,
  TableBody,
  TableRow,
  TableColumn,
  Button,
  TablePagination
} from 'react-md'

import { sort } from '../../utils/baseTools'
import TableProgressControl from './tableProgressControl'

class BaseTable extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      sortedBy: {},
      rows: [],
      fetching: false,
      rowsPerPage: props.rowsPerPage || 50
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', ::this.listenToScrollEvent)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', ::this.listenToScrollEvent)
  }

  listenToScrollEvent() {
    // const viewPortWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    // const viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)

    // const body = document.body
    // const docEl = document.documentElement

    // const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop
    // const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft

    // const clientTop = docEl.clientTop || body.clientTop || 0
    // const clientLeft = docEl.clientLeft || body.clientLeft || 0

    // const { baseId } = this.props

    // if (viewPortWidth > 1024) { // is desktop
    //   const box = document.querySelector(`#${baseId} .md-table-body`).getBoundingClientRect()

    //   // for header
    //   const header = document.querySelector(`#${baseId} .md-table-header`)
    //   if (box.top <= 64) {
    //     $(header).addClass('fixed-table-header')

    //     const tableRows = [ ...document.querySelector('#Users tbody tr').childNodes]
    //     tableRows.map((column, index) => {
    //       // [...document.querySelector('#Users thead tr').childNodes][index].style.width = `${column.offsetWidth}px`
    //       console.log('@column', column)
    //     })
    //     console.log('@test', $(box).find('tr'))


    //       [ ...document.querySelector('#Users tbody tr').childNodes].map(item=>{
    //         console.log(`@item`, window.getComputedStyle(item, null).getPropertyValue('width'))
    //         const test = window.getComputedStyle(item, null).getPropertyValue('padding').split(' ')
    //         console.log('@padding left', test[1])
    //         console.log('@padding left', test[3])
    //         console.log(item)
    //       })

    //   } else {
    //     $(header).removeClass('fixed-table-header')
    //   }

    //   // for footer
    //   const paginator = document.querySelector(`#${baseId} .md-table-footer`)
    //   const offsetTop = box.top + scrollTop - clientTop
    //   if ((box.height - scrollTop - offsetTop) < 300) {
    //     $(paginator).removeClass('fixed-table-footer')
    //   } else {
    //     $(paginator).addClass('fixed-table-footer')
    //   }
    // }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!nextProps.tableData) {
      return {
        fetching: true
      }
    }

    return {
      rows: nextProps.tableData,
      fetching: false
    }
  }


  actionButtonsConstruct(buttons, customButtons) {
    if (customButtons) {
      return customButtons
    }
    return buttons.map((button, i)=>(
      <Button icon key={i}
        className={button.class_name}
        onClick={button.handler}
        tooltipLabel={button.label}
        tooltipPosition='top'
      >{ button.icon }</Button>
    ))
  }

  handleTableHeadClick(columnObject) {
    const { rows, sortedBy } = this.state
    let sorted = !sortedBy[columnObject.key]
    sortedBy[columnObject.key] = sorted

    if (this.props.handleSorting) {
      columnObject.direction = sorted ? 'desc' : 'asc'
      this.props.handleSorting(columnObject)
    }

    this.setState({
      rows: sort(rows, columnObject.key, sorted),
      sortedBy
    })
  }

  handlePagination(start, rowsPerPage, page, data) {
    if (this.props.handlePagination) {
      this.props.handlePagination(start, rowsPerPage, page)
      this.setState({ rowsPerPage, page })
    } else {
      let rowData = []
      if (data) {
        rowData = data
      } else {
        rowData = this.props.tableData
      }

      this.setState({
        rows: rowData.slice(start, start + rowsPerPage),
        page,
        rowsPerPage
      })
    }
  }

  render() {
    const { columns
      , baseId
      , plain
      , rowCount
      , tableHeight = 500 } = this.props

    const { sortedBy
      , rows
      , fetching
      , page
      , rowsPerPage } = this.state

    const tableHead = columns.map((column, i)=>{
      if (column.sortable) {
        return (<TableColumn
          key={`base-tablehead-column-${i}`}
          role='button'
          sorted={sortedBy[column.key] ? sortedBy[column.key] : false}
          onClick={this.handleTableHeadClick.bind(this, column)}
        >{ column.name }</TableColumn>)
      }
      return <TableColumn key={`base-tablehead-column-${i}`}>{ column.name }</TableColumn>
    })

    const tableBody = rows.map((row, i)=>{
      return (<TableRow key={`base-tablebody-row-${i}`}>
        {
          columns.map((column, c)=>{
            if (column.type && column.type === 'actions') {
              return (<TableColumn key={`base-tablebody-column-${c}`}>
                { this.actionButtonsConstruct(column.actions, column.customActions) }
              </TableColumn>)
            }
            return (<TableColumn key={`base-tablebody-column-${c}`}>
              { column.format ? column.format(column.key, row, i, rows) : String(row[column.key]) }
            </TableColumn>)
          })
        }
      </TableRow>)
    })

    return (<div id={baseId}>
      <DataTable plain={plain}
        baseId={baseId}
        fixedHeader
        fixedFooter
        fixedHeight={tableHeight}>
        <TableHeader>
          <TableRow>
            { tableHead }
          </TableRow>
        </TableHeader>
        <TableBody>
          { fetching || rows.length <= 0 ? <TableProgressControl fetching={fetching} rows={rows} /> : tableBody }
        </TableBody>
        <TablePagination
          rows={rowCount}
          page={page}
          rowsPerPageItems={[10, 50, 100, 150, 250, 500]}
          rowsPerPage={rowsPerPage}
          onPagination={::this.handlePagination}
        />
      </DataTable>
    </div>)
  }
}

export default BaseTable
