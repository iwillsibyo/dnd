import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Snackbar, Button } from 'react-md'
import autobind from 'class-autobind'

import actions from '../redux/actions'

@connect(state=>(state))
class Sample extends Component {
  constructor(props) {
    super(props)
    autobind(this)
    this.state = { toasts: [], autohide: true }
  }

  addToast(text, action, autohide = true) {
    const toasts = this.state.toasts.slice()
    toasts.push({ text, autohide })
    this.setState({ toasts })
  }

  dismissToast() {
    const [, ...toasts] = this.state.toasts
    this.setState({ toasts })
  }

  toastHello() {
    this.addToast('Hello, World!')
  }

  makeAuthReq() {
    this.props.dispatch(actions.HelloAPI())
  }

  render() {
    const { toasts, autohide } = this.state
    const { error } = this.props
    return (
      <div className='App'>
        <p className='App-intro'>
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <Button raised onClick={this.toastHello}>
          Toast Hello, World!
        </Button>

        <Button raised onClick={this.makeAuthReq}>
          Make An Authenticated Request
        </Button>
        <p>api message: { error.apiMessage }</p>

        <Snackbar
          id='example-snackbar'
          toasts={toasts}
          autohide={autohide}
          onDismiss={this.dismissToast.bind(this)}
        />
      </div>
    )
  }
}
export default Sample
