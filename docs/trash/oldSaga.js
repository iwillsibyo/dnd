import { call, put, takeLatest } from 'redux-saga/effects'
import axios from '../../utils/axios'
import {
  REQUESTED
} from '../types'

function *helloAPI() {
  let config = {
    headers: {
      test: 'hahaah'
    },
    method: 'get',
    url: 'http://localhost:3003/hello-api'
  }

  try {
    const result = yield call(axios, config, 'HELLO_API')
    yield put(result)
  } catch (error) {
    console.error('UserList Error:', error)
  }
}

module.exports = function *() {
  yield takeLatest('HELLO_API' + REQUESTED, helloAPI)
}
