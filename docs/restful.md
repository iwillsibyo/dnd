// get entity grid
endpoint: /entity/grid
method: get
params: query={json string}
response: /* json object  { data: [], count: 88888 } */

// get entity record
endpoint: /entity/:id
method: get
params: id
response: /* json object of entity details */

// create entity record
endpoint: /entity
method: post
params: /* tbd */
response: /* json object of request data with the returned id from api */

//update entity record
endpoint: /entity/:id
method: put
params: id

// get distinct field values
endpoint: /:entity/field/:field
method: get
params: entity, field
response: /* arrays of string */
